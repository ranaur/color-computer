# Description

BASIC program that generates 6 random numbers from 1 to 60, print, sort the output and print again.

Line 80 can change to two different sorting routines at 1100 (bubble sort) and 1200 (shell sort).

To use bubble sort:
80 GOSUB 1100

To use shell sort:
80 GOSUB 1200

# Files

loteria-asc.cas: CAS file of save as ASCII
loteria.cas: CAS file (Binary)
loteria.dsk: Disk file
loteria.txt: Program as text file (print dump)
readme.md: this file

# Usage

To load the TXT (on XRoar):

xroar -load-text loteria.txt -machine coco

To load the CAS or DSK just plug the cassete and CLOAD or the disk and LOAD"LOTERIA


