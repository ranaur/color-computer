# Color Computer

My personal projects for the Color Computer (TRS-80 Color). Things hera are very crude, since I use the repository to preserve the sources made in my [CoCoPI](https://coco-pi.com/).

## Subprojects

* 20221007 Loteria: Simple Basic program to generate 6 1 to 60 random numbers and sort them 
* 20220215 Autococo: scripts to automaticaly download files com the [color computer archive](http://colorcomputerarchive.com) and run them.
